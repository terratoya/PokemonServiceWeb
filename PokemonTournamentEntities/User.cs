﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class User : EntityObject
    {
        public String Name { get; set; }        
        public String FirstName { get; set; }        
        public String Login { get; set; }
        public String Password { get; set; }


        public User() : base()
        {

        }
        public User(String name, String firstName,String login, String password) : base()
        {
            Name = name;
            FirstName = firstName;
            Login = login;
            Password = password;
        }

    }
}
