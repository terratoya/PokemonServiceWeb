﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class Characteristic
    {
        public EAttribute Attribute { get; set; }
        public int Value { get; set; }
        
        public Characteristic()
        {

        }

        public Characteristic(EAttribute attribute, int value)
        {
            Attribute = attribute;
            Value = value;
        }

        public override string ToString()
        {
            return Attribute.ToString() +" "+Value.ToString();
        }
    }
}
