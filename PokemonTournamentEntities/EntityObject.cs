﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public abstract class EntityObject
    {
        public int Id { get; set; }
        private static int currentId = 1;

        public EntityObject()
        {
            Id = currentId;
            currentId++;
        }
        
        public override int GetHashCode()
        {
            return this.Id;
        }
    }
}
