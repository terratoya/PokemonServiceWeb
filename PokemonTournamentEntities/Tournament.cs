﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class Tournament : EntityObject
    {
        public List<Match> Matchs { get; set; }
        public string Name { get; set; }

        public Tournament() : base()
        {

        }

        public Tournament(string name, List<Match> matchs) : base()
        {
            Matchs = matchs;
            Name = name;
        }
    }
}
