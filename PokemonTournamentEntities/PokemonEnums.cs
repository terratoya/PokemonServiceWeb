﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public enum EType
    {
        Water,
        Fire,
        Earth,
        Insect,
        Poison,
        Plant,
        Electric,
        Ice,
        Ground,
        Flying,
        Neutral
    }

    public enum EAttribute
    {
        Strengh,
        Defense,
        Speed,
        Life
    }

    public enum ETournamentPhase
    {
        Eighthfinals,
        Quarterfinals,
        Semifinals,
        Final
    }
}
