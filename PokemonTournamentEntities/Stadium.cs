﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonTournamentEntities
{
    public class Stadium : EntityObject
    {
        public EType Type { get; set; }
        public int NbPlaces { get; set; }
        public string Name { get; set; }

        public Stadium() : base()
        {

        }

        public Stadium(string name, int nbPlaces, EType type) : base()
        {
            Type = type;
            NbPlaces = nbPlaces;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
