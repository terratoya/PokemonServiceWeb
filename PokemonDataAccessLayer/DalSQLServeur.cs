﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;
using System.Data;
using System.Data.SqlClient;

namespace PokemonDataAccessLayer
{
    class DalSQLServeur : IDal
    {
        private String _connexion;

        public String Connexion
        {
            get { return _connexion; }
            set { _connexion = value; }
        }

        /*
         * 
         * CONSTRUCTEUR
         * 
         * */
        public DalSQLServeur(String connexion)
        {
            Connexion = connexion;
           
        }
        #region CommunicationBD
        /*
         * FONCTION DE COMMUNICATION AVEC LA BASE
         * */
        private DataTable Select(string request)
        {
            DataTable results = new DataTable();

            using (SqlConnection sqlConnection = new SqlConnection(Connexion))
            {
                SqlCommand sqlCommand           = new SqlCommand(request, sqlConnection);
                SqlDataAdapter sqlDataAdapter   = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(results); 
            }

            return results;
        }
        private int Update(string request, DataTable data)
        {
            int result = 0;
          
            using (SqlConnection sqlConnection = new SqlConnection(Connexion))
            {
                //SqlTransaction myTrans = sqlConnection.BeginTransaction();
                try
                {
                    //SqlTransaction myTrans = sqlConnection.BeginTransaction();
                    //SqlCommand sqlCommand = new SqlCommand(request, sqlConnection, myTrans);
                    SqlCommand sqlCommand               = new SqlCommand(request, sqlConnection);
                    SqlDataAdapter sqlDataAdapter       = new SqlDataAdapter(sqlCommand);

                    SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                    sqlDataAdapter.UpdateCommand        = sqlCommandBuilder.GetUpdateCommand();
                    sqlDataAdapter.InsertCommand        = sqlCommandBuilder.GetInsertCommand();
                    sqlDataAdapter.DeleteCommand        = sqlCommandBuilder.GetDeleteCommand();

                    sqlDataAdapter.MissingSchemaAction  = MissingSchemaAction.AddWithKey;

                    result                              = sqlDataAdapter.Update(data);
                    if (result != 0)
                        Console.WriteLine("Successfully to update the table.");
                }
                catch(Exception e)
                {
                    //myTrans.Rollback(); 
                }
            }
            return result;
        }
        #endregion

        #region Stade
        /*
         * FONCTION SUR LES STADES
         * */
        public void AddStadium(Stadium stadium)
        {
            int result      = 0;
            string request  = "SELECT * FROM STADE";
            DataTable items = Select(request);
            DataRow row     = items.NewRow();

            row["Id"]       = Convert.ToInt32(items.Rows[items.Rows.Count - 1]["Id"]) + 1;
            row["Nom"]      = stadium.Name;
            row["Nbplace"]  = stadium.NbPlaces;
            row["Type"]     = stadium.Type.ToString();

            items.Rows.Add(row);
            result          = Update(request, items);
            requestCheck(result,"Error when adding a new Stadium");   
        }
        public void RemoveStadium(Stadium stadium)
        {
            int result      = 0;
            string request  = "SELECT * FROM STADE";
            DataTable items = Select(request);

            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == stadium.Id)
                {
                    items.Rows[i].Delete();
                }
            }

            result = Update(request, items);
            requestCheck(result, "Error when removing a Stadium");
        }
        public void UpdateStadium(Stadium stadium)
        {
            int result      = 0;
            string request  = "SELECT * FROM STADE";
            DataTable items = Select(request);

            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == stadium.Id)
                {
                    dr["Nom"]       = stadium.Name;
                    dr["NbPlace"]   = stadium.NbPlaces;
                    dr["Type"]      = stadium.Type.ToString();
                }
                    
            }
            result = Update(request, items);
            requestCheck(result, "Error when updating a Stadium");
        }
        public List<Stadium> GetAllStades()
        {
            List<Stadium> list = new List<Stadium>();
            DataTable items    = Select("SELECT * FROM Stade");

            foreach (DataRow item in items.Rows)
            {
                list.Add(GetStadium(item));
            }

            return list;
        }
        private Stadium GetStadium(DataRow item)
        {
            Stadium stadium     = new Stadium();
            stadium.Id          = Convert.ToInt32(item["Id"]);
            stadium.Name        = item["Nom"].ToString();
            stadium.NbPlaces    = Convert.ToInt32(item["NbPlace"]);
            stadium.Type        = (EType) Enum.Parse(typeof(EType), item["Type"].ToString());
            return stadium;
        }
#endregion

        #region Caractéristique
        /*
         * 
         * FONCTION SUR LES CARACTERISTIQUES
         * 
         * */
        public List<Characteristic> GetAllCharacteristics()
        {
            List<Characteristic> list   = new List<Characteristic>();
            DataTable items             = Select("SELECT * FROM Characteristic");

            foreach (DataRow item in items.Rows)
            {
                list.Add(GetCharacteristic(item));
            }

            return list;
        }
        public List<Characteristic> GetAllCharacteristicsByPokemon(int IdPokemon)
        {
            List<Characteristic> list   = new List<Characteristic>();
            DataTable items             = Select("SELECT * FROM Characteristic WHERE IdPokemon = "+IdPokemon);

            foreach (DataRow item in items.Rows)
            {
                list.Add(GetCharacteristic(item));
            }

            return list;
        }
        private Characteristic GetCharacteristic(DataRow item)
        {
            Characteristic charac   = new Characteristic();
            charac.Attribute        = (EAttribute) Enum.Parse( typeof(EAttribute), item["Attribut"].ToString());
            charac.Value            = Convert.ToInt32(item["Value"]);
            return charac;
        }
#endregion

        #region Type
        /*
         * 
         * FONCTION SUR LES TYPES DE POKEMONS
         * 
         * */
        public List<EType> GetALLTypesByPokemon(int idPokemon)
        {
            List<EType> list    = new List<EType>();
            DataTable items     = Select("SELECT * FROM TypesPokemon WHERE IdPokemon = " + idPokemon);

            foreach (DataRow item in items.Rows)
            {
                list.Add(GetType(item));
            }

            return list;
        }
        private EType GetType(DataRow item)
        {
            EType type = (EType) Enum.Parse( typeof(EType), item["Type"].ToString());
            return type;
        }
        #endregion

        #region Pokemon
        /*
         * 
         * FONCTION SUR LES POKEMONS
         * 
         * */
        public void AddPokemon(Pokemon  pokemon)
        {
            int result      = 0;
            string request  = "SELECT * FROM POKEMON";
            DataTable items = Select(request);
            DataRow row     = items.NewRow();

            row["Id"]       = Convert.ToInt32(items.Rows[items.Rows.Count - 1]["Id"]) + 1;
            row["Nom"]      = pokemon.Name;
            row["Strengh"]  = pokemon.Strengh;
            row["Speed"]    = pokemon.Speed;
            row["Defense"]  = pokemon.Defense;
            row["Life"]     = pokemon.Life;

            items.Rows.Add(row);
            result          = Update(request, items);
            requestCheck(result, "Error when adding a new Pokemon");
        }
        public void RemovePokemon(Pokemon pokemon)
        {
            int result      = 0;
            string request  = "SELECT * FROM POKEMON";
            DataTable items = Select(request);

            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == pokemon.Id)
                {
                    items.Rows[i].Delete();
                }
            }

            result = Update(request, items);
            requestCheck(result, "Error when removing a Pokemon");
        }
        public void UpdatePokemon(Pokemon pokemon)
        {
            int result      = 0;
            string request  = "SELECT * FROM POKEMON";
            DataTable items = Select(request);

            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == pokemon.Id)
                {
                    dr["Nom"]       = pokemon.Name;
                    dr["Strengh"]   = pokemon.Strengh;
                    dr["Speed"]     = pokemon.Speed;
                    dr["Defense"]   = pokemon.Defense;
                    dr["Life"]      = pokemon.Life;
                }
            }

            result = Update(request, items);
            requestCheck(result, "Error when updating a Pokemon");
        }
        public List<Pokemon> GetAllPokemons()
        {
            List<Pokemon> list = new List<Pokemon>();
            DataTable items = Select("SELECT * FROM Pokemon") ;

            foreach (DataRow item in items.Rows)
            {
                list.Add(GetPokemon(item));
            }

            return list;
        }
        private Pokemon GetPokemon(DataRow item)
        {
            Pokemon pokemon    = new Pokemon();
            pokemon.Id         = Convert.ToInt32(item["Id"]);
            pokemon.Name       = item["Name"].ToString();
            pokemon.Strengh    = Convert.ToInt32(item["Strengh"]);
            pokemon.Life       = Convert.ToInt32(item["Life"]);
            pokemon.Defense    = Convert.ToInt32(item["Defense"]);
            pokemon.Speed      = Convert.ToInt32(item["Speed"]);
            //poke.Characteristics = GetAllCharacteristicsByPokemon(poke.Id);
            pokemon.Types = GetALLTypesByPokemon(pokemon.Id);
            return pokemon;
        }

        #endregion

        #region Match

        /*
         * 
         * FONCTION SUR LES MATCHS
         * 
         * */
        public void AddMatch(Match match)
        {

        }
        public void RemoveMatch(Match match)
        {

        }
        public void UpdateMatch(Match match)
        {

        }
        public List<Match> GetAllMatchs()
        {
            return new List<Match>();
        }

        #endregion

        #region User
        /*
         * 
         * FONCTION SUR LES UTILISATEURS
         * 
         * */
        public void AddUser(User user)
        {
            int result = 0;
            string request = "SELECT * FROM USER";
            DataTable items = Select(request);
            DataRow row = items.NewRow();
            row["Id"] = Convert.ToInt32(items.Rows[items.Rows.Count - 1]["Id"]) + 1;
            row["Name"] = user.Name;
            row["FirstName"] = user.FirstName;
            row["Login"] = user.Login;
            row["Password"] = user.Password;
            items.Rows.Add(row);
            result = Update(request, items);
            requestCheck(result, "Error when adding a new User");
        }
        public void RemoveUser(User user)
        {
            int result = 0;
            string request = "SELECT * FROM USER";
            DataTable items = Select(request);
            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == user.Id)
                    items.Rows.Remove(dr);
            }
            result = Update(request, items);
            requestCheck(result, "Error when removing an User");
        }
        public void UpdateUser(User user)
        {
            int result = 0;
            string request = "SELECT * FROM USER";
            DataTable items = Select(request);
            for (int i = items.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = items.Rows[i];
                if (Convert.ToInt32(dr["Id"]) == user.Id)
                {
                    dr["Name"] = user.Name;
                    dr["FirstName"] = user.FirstName;
                    dr["Login"] = user.Login;
                    dr["Password"] = user.Password;
                }

            }
            result = Update(request, items);
            requestCheck(result, "Error when updating an User");
        }
        public User GetUserByLogin(String login)
        {
            string str = "SELECT * FROM UserApplication WHERE Login LIKE '" + login + "'";
            User user = new User("","","","");
            DataTable items = Select(str);
            if (items.Rows.Count > 0)
            {
                Console.WriteLine(str);
                DataRow item = items.Rows[items.Rows.Count - 1];
                user.Name = item["Name"].ToString();
                user.FirstName = item["FirstName"].ToString();
                user.Login = login;
                user.Password = item["Password"].ToString();
                user.Id = Convert.ToInt32(item["Id"]);
                Console.WriteLine(user.ToString());
            }
            return user;
        }
        #endregion

        private void requestCheck(int result,string mesg)
        {
            if (result == 0)
                throw new Exception(mesg);
        }
        public void CreateWorld()
        {
            
        }

    }
}
