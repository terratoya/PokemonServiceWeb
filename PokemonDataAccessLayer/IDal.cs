﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;

namespace PokemonDataAccessLayer
{
    public interface IDal
    {
        /*
         * FONCTION SUR LES STADES
         */
        void AddStadium(Stadium stadium);
        void RemoveStadium(Stadium stadium);
        void UpdateStadium(Stadium stadium);
        List<Stadium> GetAllStades();

        /*
         * FONCTION SUR LES CARACTERISTIQUES
         */
        List<Characteristic> GetAllCharacteristics();
        List<Characteristic> GetAllCharacteristicsByPokemon(int IdPokemon);

        /*
         * FONCTION SUR LES POKEMONS
         * */
        void AddPokemon(Pokemon pokemon);
        void RemovePokemon(Pokemon pokemon);
        void UpdatePokemon(Pokemon pokemon);
        List<Pokemon> GetAllPokemons();

        /*
         *FONCTION SUR LES TYPES des Pokemons
         * */
        List<EType> GetALLTypesByPokemon(int idPokemon);

        /*
         * FONCTION SUR LES MATCHS
         * */
        void AddMatch(Match match);
        void RemoveMatch(Match match);
        void UpdateMatch(Match match);
        List<Match> GetAllMatchs();

        /*
         * FONCTION SUR LES UTILISATEURS
         * */
        void AddUser(User user);
        void RemoveUser(User user);
        void UpdateUser(User user);
        User GetUserByLogin(String login);

        void CreateWorld();

    }
}
