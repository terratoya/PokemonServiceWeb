﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;

namespace PokemonDataAccessLayer
{
    class DalSTubbed : IDal
    {
        private List<Pokemon> listPokemons = new List<Pokemon>();
        private List<Stadium> listStadiums = new List<Stadium>();
        private List<Match> listMatchs = new List<Match>();
        private List<Characteristic> listCharacteristics = new List<Characteristic>();
        private List<User> listUsers = new List<User>();

        public void AddStadium(Stadium stadium)
        {
            listStadiums.Add(stadium);
        }

        public void RemoveStadium(Stadium stadium)
        {
            listStadiums.Remove(stadium);
        }

        public void AddPokemon(Pokemon pokemon)
        {
            listPokemons.Add(pokemon);
        }

        public void RemovePokemon(Pokemon pokemon)
        {
            listPokemons.Remove(pokemon);
        }

        public List<Characteristic> GetAllCharacteristics()
        {
            throw new NotImplementedException();
        }

        public List<Pokemon> GetAllPokemons()
        {
            return listPokemons;
        }

        public List<Stadium> GetAllStades()
        {
            return listStadiums;
        }

        public List<Match> GetAllMatchs()
        {
            return listMatchs;
        }

        public List<Pokemon> GetAllPokemonsFromType(EType type)
        {
            return GetAllPokemons().Where(p => p.Types.Contains(type)).ToList();
        }

        public User GetUserByLogin(string login)
        {
            return listUsers.Where(u => u.Login == login).FirstOrDefault();
        }

        public void CreateWorld()
        {
            listCharacteristics.Add(new Characteristic(EAttribute.Strengh, 100));
            listCharacteristics.Add(new Characteristic(EAttribute.Defense, 50));
            listCharacteristics.Add(new Characteristic(EAttribute.Speed, 80));
            listCharacteristics.Add(new Characteristic(EAttribute.Life, 200));

            /*Pokemon p1 = new Pokemon("Electhor", listCharacteristics, new List<EType>() { EType.Electric, EType.Flying });
            Pokemon p2 = new Pokemon("Brasegali", listCharacteristics, new List<EType>() { EType.Fire });
            Pokemon p3 = new Pokemon("Onigali", listCharacteristics, new List<EType>() { EType.Ice });
            Pokemon p4 = new Pokemon("Lucario", listCharacteristics, new List<EType>() { EType.Ground });
            Pokemon p5 = new Pokemon("Tortank", listCharacteristics, new List<EType>() { EType.Water, EType.Ground });
            Pokemon p6 = new Pokemon("Germinion", listCharacteristics, new List<EType>() { EType.Plant });
            Pokemon p7 = new Pokemon("Groudon", listCharacteristics, new List<EType>() { EType.Fire, EType.Ground });
            Pokemon p8 = new Pokemon("Kyogre", listCharacteristics, new List<EType>() { EType.Water, EType.Flying });*/
            Pokemon p1 = new Pokemon("Electhor", 200,100,50,20, new List<EType>() { EType.Electric, EType.Flying });
            Pokemon p2 = new Pokemon("Brasegali", 200, 100, 50, 20, new List<EType>() { EType.Fire });
            Pokemon p3 = new Pokemon("Onigali", 200, 100, 50, 20, new List<EType>() { EType.Ice });
            Pokemon p4 = new Pokemon("Lucario", 200, 100, 50, 20, new List<EType>() { EType.Ground });
            Pokemon p5 = new Pokemon("Tortank", 200, 100, 50, 20, new List<EType>() { EType.Water, EType.Ground });
            Pokemon p6 = new Pokemon("Germinion", 200, 100, 50, 20, new List<EType>() { EType.Plant });
            Pokemon p7 = new Pokemon("Groudon", 200, 100, 50, 20, new List<EType>() { EType.Fire, EType.Ground });
            Pokemon p8 = new Pokemon("Kyogre", 200, 100, 50, 20, new List<EType>() { EType.Water, EType.Flying });
            listPokemons.Add(p1);
            listPokemons.Add(p2);
            listPokemons.Add(p3);
            listPokemons.Add(p4);
            listPokemons.Add(p5);
            listPokemons.Add(p6);
            listPokemons.Add(p7);
            listPokemons.Add(p8);

            Stadium s1 = new Stadium("Stadium neutre", 75000, EType.Neutral);
            Stadium s2 = new Stadium("Stadium éclair", 50000, EType.Electric);
            Stadium s3 = new Stadium("Stadium aquatique", 90000, EType.Water);
            Stadium s4 = new Stadium("Stadium volcan", 60000, EType.Fire);

            listStadiums.Add(s1);
            listStadiums.Add(s2);
            listStadiums.Add(s3);
            listStadiums.Add(s4);

            Match.init();
            Match m1 = new Match(ETournamentPhase.Quarterfinals, p1, p2, s2);
            Match m2 = new Match(ETournamentPhase.Quarterfinals, p3, p4, s1);
            Match m3 = new Match(ETournamentPhase.Quarterfinals, p5, p6, s3);
            Match m4 = new Match(ETournamentPhase.Quarterfinals, p7, p8, s4);
            Match m5 = new Match(ETournamentPhase.Semifinals, p1, p3, s4);
            Match m6 = new Match(ETournamentPhase.Semifinals, p5, p7, s2);
            Match m7 = new Match(ETournamentPhase.Final, p1, p5, s1);
            listMatchs.Add(m1);
            listMatchs.Add(m2);
            listMatchs.Add(m3);
            listMatchs.Add(m4);
            listMatchs.Add(m5);
            listMatchs.Add(m6);
            listMatchs.Add(m7);

            User u1 = new User("Maxime", "Chomont", "", "");
            listUsers.Add(u1);
        }

        public void UpdateStadium(Stadium stadium)
        {
            Stadium s = listStadiums.Where(elt => elt.Id == stadium.Id).FirstOrDefault();
            if (s != null)
            {
                s.Name = stadium.Name;
                s.NbPlaces = stadium.NbPlaces;
                s.Type = stadium.Type;
            }
            else
            {
                throw new Exception("Error update stadium : stadium doesn't exist (" + stadium.ToString() + ")");
            }
        }

        public List<Characteristic> GetAllCharacteristicsByPokemon(int IdPokemon)
        {
            throw new NotImplementedException();
        }

        public void UpdatePokemon(Pokemon pokemon)
        {
            Pokemon p = listPokemons.Where(elt => elt.Id == pokemon.Id).FirstOrDefault();
            if(p != null)
            {
                p.Name = pokemon.Name;
                p.Life = pokemon.Life;
                p.Strengh = pokemon.Strengh;
                p.Defense = pokemon.Defense;
                p.Speed = pokemon.Speed;
                p.Types = pokemon.Types;
            }
            else
            {
                throw new Exception("Error update pokemon : pokemon doesn't exist (" + pokemon.ToString() + ")");
            }
        }

        public List<EType> GetALLTypesByPokemon(int idPokemon)
        {
            throw new NotImplementedException();
        }

        public void AddMatch(Match match)
        {
            throw new NotImplementedException();
        }

        public void RemoveMatch(Match match)
        {
            throw new NotImplementedException();
        }

        public void UpdateMatch(Match match)
        {
            throw new NotImplementedException();
        }

        public void AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public void RemoveUser(User user)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
