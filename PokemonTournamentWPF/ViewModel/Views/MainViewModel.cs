﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonBusinessLayer;
using PokemonTournamentEntities;
using System.Collections.ObjectModel;
using PokemonTournamentWPF.UsersControls;
using System.Windows.Controls;
using System.Windows;
using PokemonTournamentWPF.View;

namespace PokemonTournamentWPF.ViewModel.Views
{
    //This is class is used to display objects in the main window
    public class MainViewModel : ViewModelBase
    {
        private BusinessManager manager;

        #region "Propriétés accessibles, mappables par la View"
        private ObservableCollection<PokemonView> pokemons;
        public ObservableCollection<PokemonView> Pokemons
        {
            get { return pokemons; }
            set
            {
                pokemons = value;
                base.OnPropertyChanged("Pokemons");
            }
        }

        private PokemonView selectedPokemon;
        public PokemonView SelectedPokemon
        {
            get { return selectedPokemon; }
            set
            {
                selectedPokemon = value;
                base.OnPropertyChanged("SelectedPokemon");
            }
        }

        private ObservableCollection<StadiumView> stadiums;
        public ObservableCollection<StadiumView> Stadiums
        {
            get { return stadiums; }
            set
            {
                stadiums = value;
                base.OnPropertyChanged("Stadiums");
            }
        }

        private StadiumView selectedStadium;
        public StadiumView SelectedStadium
        {
            get { return selectedStadium; }
            set
            {
                selectedStadium = value;
                base.OnPropertyChanged("SelectedStadium");
            }
        }

        private ObservableCollection<MatchView> matchs;
        public ObservableCollection<MatchView> Matchs
        {
            get { return matchs; }
            set
            {
                matchs = value;
                base.OnPropertyChanged("Matchs");
            }
        }

        private ObservableCollection<CheckBox> pokemonsFiltering;
        public ObservableCollection<CheckBox> PokemonsFiltering
        {
            get { return pokemonsFiltering; }
            set
            {
                pokemonsFiltering = value;
                base.OnPropertyChanged("PokemonsFiltering");
            }
        }

        private Visibility pokemonViewVisibility;
        public Visibility PokemonViewVisibility
        {
            get { return pokemonViewVisibility; }
            set
            {
                pokemonViewVisibility = value;
                base.OnPropertyChanged("PokemonViewVisibility");
            }
        }

        private Visibility stadiumViewVisibility;
        public Visibility StadiumViewVisibility
        {
            get { return stadiumViewVisibility; }
            set
            {
                stadiumViewVisibility = value;
                base.OnPropertyChanged("StadiumViewVisibility");
            }
        }

        private Visibility matchViewVisibility;
        public Visibility MatchViewVisibility
        {
            get { return matchViewVisibility; }
            set
            {
                matchViewVisibility = value;
                base.OnPropertyChanged("MatchViewVisibility");
            }
        }
        #endregion

        public MainViewModel()
        {
            manager = new BusinessManager();

            pokemons = new ObservableCollection<PokemonView>();
            stadiums = new ObservableCollection<StadiumView>();
            matchs = new ObservableCollection<MatchView>();
            pokemonsFiltering = new ObservableCollection<CheckBox>();

            pokemonViewVisibility = Visibility.Visible;
            stadiumViewVisibility = Visibility.Collapsed;
            matchViewVisibility = Visibility.Collapsed;

            InitPokemonsView();
            InitPokemonsFilteringView();
            InitStadiumsView();
            InitMatchsView();
        }

        public void AddStadiumCollection(Stadium stadium)
        {
            stadiums.Add(new StadiumView(stadium));
        }

        public void AddPokemonCollection(Pokemon pokemon)
        {
            pokemons.Add(new PokemonView(pokemon));
        }

        #region "Initialize all view with pokemons/stadiums/matchs"
        public void InitPokemonsView(List<EType> filters = null)
        {
            List<Pokemon> pokemonsToDisplay;

            if(filters == null){
                pokemonsToDisplay = manager.GetAllPokemons();
            } else {
                pokemonsToDisplay = manager.GetAllPokemonsByTypes(filters);
            }

            pokemons.Clear();
            foreach (Pokemon p in pokemonsToDisplay)
            {
                pokemons.Add(new PokemonView(p));
            }            
        }
        public void InitPokemonsFilteringView()
        {
            pokemonsFiltering.Clear();
            foreach (EType type in Enum.GetValues(typeof(EType)))
            {
                CheckBox c = new CheckBox();
                c.Checked += cb_filtering_pokemons;
                c.Unchecked += cb_filtering_pokemons;
                c.Content = type;
                pokemonsFiltering.Add(c);
            }
        }
        private void cb_filtering_pokemons(object sender, RoutedEventArgs e)
        {
            List<EType> filters = pokemonsFiltering.Where(cb => (bool)cb.IsChecked).Select(cb => (EType)cb.Content).ToList();
            if (filters.Count != 0)
            {
                InitPokemonsView(filters);
            }
            else
            {
                InitPokemonsView();
            }

        }
        public void InitStadiumsView()
        {
            stadiums.Clear();
            foreach (Stadium s in manager.GetAllStadiums())
            {
                stadiums.Add(new StadiumView(s));
            }
        }
        public void InitMatchsView()
        {
            matchs.Clear();
            foreach (Match m in manager.GetAllMatchs())
            {
                matchs.Add(new MatchView(m));
            }
        }
        #endregion

        // =============================================================================
        
        #region "Command add a pokemon in the collection"
        private RelayCommand _addPokemonCommand;
        public System.Windows.Input.ICommand AddPokemonCommand
        {
            get
            {
                if (_addPokemonCommand == null)
                {
                    _addPokemonCommand = new RelayCommand(
                        () => this.AddPokemon(),
                        () => this.CanAddPokemon()
                        );
                }
                return _addPokemonCommand;
            }
        }

        private bool CanAddPokemon()
        {
            return true;
        }

        private void AddPokemon()
        {
            AddPokemon window = new AddPokemon(this);
            window.ShowDialog();
        }
        #endregion

        #region "Command update a pokemon in the collection"
        private RelayCommand _updatePokemonCommand;
        public System.Windows.Input.ICommand UpdatePokemonCommand
        {
            get
            {
                if (_updatePokemonCommand == null)
                {
                    _updatePokemonCommand = new RelayCommand(
                        () => this.UpdatePokemon(),
                        () => this.CanUpdatePokemon()
                        );
                }
                return _updatePokemonCommand;
            }
        }

        private bool CanUpdatePokemon()
        {
            return true;
        }

        private void UpdatePokemon()
        {
            if(selectedPokemon != null)
            {
                AddPokemon window = new AddPokemon(this, selectedPokemon.PokemonEntity);
                window.ShowDialog();
            }
            else
            {
                MessageBox.Show("You need to select a pokemon");
            }
        }
        #endregion

        #region "Command remove a pokemon in the collection"
        private RelayCommand _removePokemonCommand;
        public System.Windows.Input.ICommand RemovePokemonCommand
        {
            get
            {
                if (_removePokemonCommand == null)
                {
                    _removePokemonCommand = new RelayCommand(
                        () => this.RemovePokemon(),
                        () => this.CanRemovePokemon()
                        );
                }
                return _removePokemonCommand;
            }
        }

        private bool CanRemovePokemon()
        {
            return true;
        }

        private void RemovePokemon()
        {
            if (selectedPokemon != null)
            {
                MessageBoxResult result = MessageBox.Show("Do you want to remove the pokemon " + selectedPokemon.PokemonEntity.Name + " ?", "Remove pokemon", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        manager.RemovePokemon(selectedPokemon.PokemonEntity);
                        pokemons.Remove(selectedPokemon);
                        MessageBox.Show("Pokemon removed", "Success", MessageBoxButton.OK, MessageBoxImage.None);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }                
            }
            else
            {
                MessageBox.Show("You need to select a pokemon");
            }
        }
        #endregion

        // =============================================================================

        #region "Command add a stadium in the collection"
        private RelayCommand _addStadiumCommand;
        public System.Windows.Input.ICommand AddStadiumCommand
        {
            get
            {
                if (_addStadiumCommand == null)
                {
                    _addStadiumCommand = new RelayCommand(
                        () => this.AddStadium(),
                        () => this.CanAddStadium()
                        );
                }
                return _addStadiumCommand;
            }
        }

        private bool CanAddStadium()
        {
            return true;
        }

        private void AddStadium()
        {
            AddStadium window = new AddStadium(this);
            window.ShowDialog();
        }
        #endregion

        #region "Command update a stadium in the collection"
        private RelayCommand _updateStadiumCommand;
        public System.Windows.Input.ICommand UpdateStadiumCommand
        {
            get
            {
                if (_updateStadiumCommand == null)
                {
                    _updateStadiumCommand = new RelayCommand(
                        () => this.UpdateStadium(),
                        () => this.CanUpdateStadium()
                        );
                }
                return _updateStadiumCommand;
            }
        }

        private bool CanUpdateStadium()
        {
            return true;
        }

        private void UpdateStadium()
        {           
            if (selectedStadium != null)
            {
                AddStadium window = new AddStadium(this, selectedStadium.StadiumEntity);
                window.ShowDialog();
            }
            else
            {
                MessageBox.Show("You need to select a stadium");
            }
        }
        #endregion

        #region "Command remove a stadium in the collection"
        private RelayCommand _removeStadiumCommand;
        public System.Windows.Input.ICommand RemoveStadiumCommand
        {
            get
            {
                if (_removeStadiumCommand == null)
                {
                    _removeStadiumCommand = new RelayCommand(
                        () => this.RemoveStadium(),
                        () => this.CanRemoveStadium()
                        );
                }
                return _removeStadiumCommand;
            }
        }

        private bool CanRemoveStadium()
        {
            return true;
        }

        private void RemoveStadium()
        {
            if(selectedStadium != null)
            {
                if (selectedStadium != null)
                {
                    MessageBoxResult result = MessageBox.Show("Do you want to remove the stadium " + selectedStadium.StadiumEntity.Name + " ?", "Remove stadium", MessageBoxButton.YesNo);
                    if (result == MessageBoxResult.Yes)
                    {
                        try
                        {
                            manager.RemoveStadium(selectedStadium.StadiumEntity);
                            stadiums.Remove(selectedStadium);
                            MessageBox.Show("Stadium removed", "Success", MessageBoxButton.OK, MessageBoxImage.None);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You need to select a stadium");
                }
            }
        }
        #endregion

        // =============================================================================

        #region "Command to save pokemons"
        private RelayCommand _savePokemonsCommand;
        public System.Windows.Input.ICommand SavePokemonsCommand
        {
            get
            {
                if (_savePokemonsCommand == null)
                {
                    _savePokemonsCommand = new RelayCommand(
                        () => this.SavePokemons(),
                        () => this.CanSavePokemons()
                        );
                }
                return _savePokemonsCommand;
            }
        }

        private bool CanSavePokemons()
        {
            return true;
        }

        private void SavePokemons()
        {
            XMLSave fenetre = new XMLSave();
            fenetre.Show();
        }
        #endregion

        // =============================================================================

        #region "Command to display pokemon view"
        private RelayCommand _displayPokemonViewCommand;
        public System.Windows.Input.ICommand DisplayPokemonViewCommand
        {
            get
            {
                if (_displayPokemonViewCommand == null)
                {
                    _displayPokemonViewCommand = new RelayCommand(
                        () => this.DisplayPokemonView(),
                        () => this.CanDisplayPokemonView()
                        );
                }
                return _displayPokemonViewCommand;
            }
        }

        private bool CanDisplayPokemonView()
        {
            return true;
        }

        private void DisplayPokemonView()
        {
            PokemonViewVisibility = Visibility.Visible;
            StadiumViewVisibility = Visibility.Collapsed;
            MatchViewVisibility = Visibility.Collapsed;
        }
        #endregion

        #region "Command to display stadium view"
        private RelayCommand _displayStadiumViewCommand;
        public System.Windows.Input.ICommand DisplayStadiumViewCommand
        {
            get
            {
                if (_displayStadiumViewCommand == null)
                {
                    _displayStadiumViewCommand = new RelayCommand(
                        () => this.DisplayStadiumView(),
                        () => this.CanDisplayStadiumView()
                        );
                }
                return _displayStadiumViewCommand;
            }
        }

        private bool CanDisplayStadiumView()
        {
            return true;
        }

        private void DisplayStadiumView()
        {
            PokemonViewVisibility = Visibility.Collapsed;
            StadiumViewVisibility = Visibility.Visible;
            MatchViewVisibility = Visibility.Collapsed;
        }
        #endregion

        #region "Command to display match view"
        private RelayCommand _displayMatchViewCommand;
        public System.Windows.Input.ICommand DisplayMatchViewCommand
        {
            get
            {
                if (_displayMatchViewCommand == null)
                {
                    _displayMatchViewCommand = new RelayCommand(
                        () => this.DisplayMatchView(),
                        () => this.CanDisplayMatchView()
                        );
                }
                return _displayMatchViewCommand;
            }
        }

        private bool CanDisplayMatchView()
        {
            return true;
        }

        private void DisplayMatchView()
        {
            PokemonViewVisibility = Visibility.Collapsed;
            StadiumViewVisibility = Visibility.Collapsed;
            MatchViewVisibility = Visibility.Visible;
        }
        #endregion
    }
}
