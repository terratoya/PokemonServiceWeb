﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonTournamentEntities;
using System.Collections.ObjectModel;

namespace PokemonTournamentWPF.ViewModel
{
    //This is class is used to display pokemons characteristics in the main window
    public class PokemonViewModel : ViewModelBase
    {
        #region "Propriétés accessibles, mappables par la View"

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    base.OnPropertyChanged("Name");
                }
            }
        }

        private ObservableCollection<EType> types;
        public ObservableCollection<EType> Types
        {
            get { return types; }
            set
            {
                types = value;
                base.OnPropertyChanged("Types");
            }
        }

        private ObservableCollection<Characteristic> characteristics;
        public ObservableCollection<Characteristic> Characteristics
        {
            get { return characteristics; }
            set
            {
                characteristics = value;
                base.OnPropertyChanged("Characteristics");
            }
        }
        #endregion

        public PokemonViewModel(Pokemon pokemon)
        {
            types = new ObservableCollection<EType>();
            characteristics = new ObservableCollection<Characteristic>();

            name = pokemon.Name;
            foreach(EType t in pokemon.Types)
            {
                types.Add(t);
            }
            foreach(Characteristic c in pokemon.Characteristics)
            {
                characteristics.Add(c);
            }

        }
    }
}
