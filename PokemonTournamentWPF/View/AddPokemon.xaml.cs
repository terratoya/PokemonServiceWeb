﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PokemonTournamentEntities;
using PokemonTournamentWPF.ViewModel.Views;

namespace PokemonTournamentWPF.View
{
    /// <summary>
    /// Interaction logic for AddPokemon.xaml
    /// </summary>
    public partial class AddPokemon : Window
    {
        AddPokemonViewModel viewModel;

        public AddPokemon(MainViewModel mainWindow)
        {
            InitializeComponent();

            viewModel = new AddPokemonViewModel(mainWindow);
            this.DataContext = viewModel;
            btn_action.Command = viewModel.AddPokemonCommand;
            btn_action.Content = "Add pokemon";
        }

        public AddPokemon(MainViewModel mainWindow, Pokemon pokemon)
        {
            InitializeComponent();

            viewModel = new AddPokemonViewModel(mainWindow,pokemon);
            this.DataContext = viewModel;
            btn_action.Command = viewModel.UpdatePokemonCommand;
            btn_action.Content = "Update pokemon";
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
