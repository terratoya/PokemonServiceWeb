﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PokemonBusinessLayer;
using PokemonTournamentEntities;

namespace PokemonTournamentWPF
{
    /// <summary>
    /// Interaction logic for AddStadium.xaml
    /// </summary>
    public partial class AddStadium : Window
    {
        private MainWindow Window;
        public AddStadium(MainWindow window)
        {
            InitializeComponent();
            Window = window;

            cb_stadiumType.ItemsSource = Enum.GetValues(typeof(EType)).Cast<EType>();
        }

        private void Button_add_Click(object sender, RoutedEventArgs e)
        {
            String name = text_stadiumName.Text;
            EType type;
            int nbPlaces;
            

            if (text_stadiumName.Text == "")
            {
                MessageBox.Show("You need to specify the stadium name");
            }
            else if(text_nbPlaces.Text == "" || !Int32.TryParse(text_nbPlaces.Text,out nbPlaces) )
            {
                MessageBox.Show("You need to specify the number of places of the stadium");
            }
            else if(cb_stadiumType.Text == "")
            {                
                MessageBox.Show("You need to specify a type for the stadium");
            }
            else
            {
                type = (EType)cb_stadiumType.SelectedItem;
                BusinessManager manager = new BusinessManager();
                manager.AddStadium(new Stadium(name, nbPlaces, type));
                Window.InitStadiumsView();

                MessageBox.Show("Stadium added");
                this.Close();
            }
        }
    }
}
