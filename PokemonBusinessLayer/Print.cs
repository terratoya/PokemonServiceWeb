﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Printing;
using System.IO;
//using Newtonsoft.Json;

namespace PokemonTournamentWPF {
    class Item {
        
            public string Font { get; set; }
            public string Color { get; set; }
            public int Size { get; set; }
            public string Align { get; set; }

        }


        public class PokemonPrinter : PrintDocument {
            public Font PrintFont { get; set; }
            public string PrintText { get; set; }
            private Brush brush;
            private StringFormat format = new StringFormat(StringFormatFlags.LineLimit);
            private static int curChar;

     

            public PokemonPrinter(string printer, string text) {
                PrinterSettings.PrinterName = printer;
                PrintText = text;
                PrintFont = new Font("Times New Roman", 12);
                brush = Brushes.Black;

        }

            protected override void OnPrintPage(PrintPageEventArgs e) {
                base.OnPrintPage(e);
                int printHeight;
                int printWidth;
                int leftMargin;
                int rightMargin;
                int lines;
                int chars;

                printHeight = base.DefaultPageSettings.PaperSize.Height - base.DefaultPageSettings.Margins.Top - base.DefaultPageSettings.Margins.Bottom;
                printWidth = base.DefaultPageSettings.PaperSize.Width - base.DefaultPageSettings.Margins.Left - base.DefaultPageSettings.Margins.Right;
                leftMargin = base.DefaultPageSettings.Margins.Left;  //X
                rightMargin = base.DefaultPageSettings.Margins.Top;  //Y

                //Create a rectangle printing are for our document
                RectangleF printArea = new RectangleF(leftMargin, rightMargin, printWidth, printHeight);

                //Use the StringFormat class for the text layout of our document


                //Fit as many characters as we can into the print area
                e.Graphics.MeasureString(PrintText.Substring(RemoveZeros(ref curChar)), PrintFont, new SizeF(printWidth, printHeight), format, out chars, out lines);

                //Print the page
                e.Graphics.DrawString(PrintText.Substring(RemoveZeros(ref curChar)), PrintFont, brush, printArea, format);

                //Increase current char count
                curChar += chars;

                //Detemine if there is more text to print, if
                //there is the tell the printer there is more coming
                if (curChar < PrintText.Length) {
                    e.HasMorePages = true;
                }
                else {
                    e.HasMorePages = false;
                    curChar = 0;
                }
            }

            public int RemoveZeros(ref int value) {
                //As 0 (ZERO) being sent to DrawString will create unexpected
                //problems when printing we need to search for the first
                //non-zero character in the string.
                while (PrintText[value] == 0) {
                    value++;
                }
                return value;
            }
        }
    }


